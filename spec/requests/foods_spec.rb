require 'rails_helper'

RSpec.describe 'ActionTrack::Foods API', type: :request do
  # initialize test data
  let!(:foods) { create_list(:food, 10) }
  let(:food_id) { foods.first.id }

  # Test suite for GET /foods
  describe 'GET /foods' do
    # make HTTP get request before each example
    before { get '/foods' }

    it 'returns foods' do
      # Note `json` is a custom helper to parse JSON responses
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  # Test suite for GET /foods/:id
  describe 'GET /foods/:id' do
    # make HTTP get request before each example
    before { get "/foods/#{food_id}" }

    context 'when the record exists' do
      it 'returns the food' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(food_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:food_id) { 100 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Food/)
      end
    end
  end

  # Test suite for POST /foods
  describe 'POST /foods' do
    # valid payload
    let(:valid_attributes) { { name: 'Avocado',
                               total_fat: 15,
                               total_carbohydrates: 9,
                               protein: 2 } }

    context 'when the request is valid' do
      before { post '/foods', params: valid_attributes }

      it 'creates a food' do
        expect(json['name']).to eq('Avocado')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before { post '/foods', params: { name: 'Avocado',
                                        total_fat: 15,
                                        total_carbohydrates: 9 } }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body)
          .to match(/Validation failed: Protein can't be blank/)
      end
    end
  end

  # Test suite for PUT /foods/:id
  describe 'PUT /foods/:id' do
    let(:valid_attributes) { { name: 'Ripe Avocado' } }

    context 'when the record exists' do
      before { put "/foods/#{food_id}", params: valid_attributes }

      it 'updates the record' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end

  # Test suite for DELETE /foods/:id
  describe 'DELETE /foods/:id' do
    before { delete "/foods/#{food_id}" }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end

end