require 'rails_helper'

RSpec.describe 'ActionTrack::Ingredients API' do
  # Initialize the test data
  let!(:meal) { create(:meal) }
  let!(:food) { create(:food) }
  let!(:ingredients) { create_list(:ingredient, 20, meal_id: meal.id, food_id:food.id) }
  let(:meal_id) { meal.id }
  let(:food_id) { food.id }
  let(:id) { ingredients.first.id }

  # Test suite for GET /meals/:meal_id/ingredients
  describe 'GET /meals/:meal_id/ingredients' do
    before { get "/meals/#{meal_id}/ingredients" }

    context 'when meal exists' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns all ingredients' do
        expect(json.size).to eq(20)
      end
    end

    context 'when meal does not exist' do
      let(:meal_id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Meal/)
      end
    end
  end

  # Test suite for GET /meals/:meal_id/ingredients/:id
  describe 'GET /meals/:meal_id/ingredients/:id' do
    before { get "/meals/#{meal_id}/ingredients/#{id}" }

    context 'when ingredient exists' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns the ingredient' do
        expect(json['id']).to eq(id)
      end
    end

    context 'when ingredient does not exist' do
      let(:id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Ingredient/)
      end
    end
  end

  # Test suite for POST /meals/:meal_id/ingredients
  describe 'POST /meals/:meal_id/ingredients' do
    let(:valid_attributes) { { amount: 200, food_id: food.id} }

    context 'when request attributes are valid' do
      before { post "/meals/#{meal_id}/ingredients", params: valid_attributes }

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when an invalid request' do
      before { post "/meals/#{meal_id}/ingredients", params: {} }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a failure message' do
        expect(response.body).to match(/Validation failed: Food must exist, Amount can't be blank/)
      end
    end
  end

  # Test suite for PUT /meals/:meal_id/ingredients/:id
  describe 'PUT /meals/:meal_id/ingredients/:id' do
    let(:valid_attributes) { { amount: 25 } }

    before { put "/meals/#{meal_id}/ingredients/#{id}", params: valid_attributes }

    context 'when item exists' do
      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end

      it 'updates the ingredient' do
        updated_item = Ingredient.find(id)
        expect(updated_item.amount).to equal(25.0)
      end
    end

    context 'when the ingredient does not exist' do
      let(:id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Ingredient/)
      end
    end
  end

  # Test suite for DELETE /meals/:meal_id/ingredients/:id
  describe 'DELETE /meals/:meal_id/ingredients/:id' do
    before { delete "/meals/#{meal_id}/ingredients/#{id}" }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end

end
