FactoryBot.define do
  factory :ingredient do
    description { Faker::StarWars.character }
    amount { Faker::Number.rand(100) }
    meal_id nil
    food_id nil
  end
end