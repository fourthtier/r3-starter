FactoryBot.define do
  factory :food do
    name { Faker::Food.name }
    total_fat { Faker::Number.rand(30) }
    total_carbohydrates { Faker::Number.rand(75) }
    protein { Faker::Number.rand(50) }
  end
end