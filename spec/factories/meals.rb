FactoryBot.define do
  factory :meal do
    name { Faker::Lorem.word }
  end
end