require 'rails_helper'

RSpec.describe Ingredient, type: :model do
  # Association test
  # ensure an ingredient record belongs to a single food record
  it { should belong_to(:food) }
  # ensure an ingredient record belongs to a single meal record
  it { should belong_to(:meal) }
  # Validation test
  # ensure column name is present before saving
  it { should validate_presence_of(:amount) }
end
