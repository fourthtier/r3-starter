require 'rails_helper'

# Test suite for the Food model
RSpec.describe Food, type: :model do
  # Association test
  # ensure Food model has a m:m relationship with the Meal model
  it { should have_many(:meals) }
  # Validation tests
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:total_fat) }
  it { should validate_presence_of(:total_carbohydrates) }
  it { should validate_presence_of(:protein) }
end