require 'rails_helper'

RSpec.describe Meal, type: :model do
  # ensure Meal model has a 1:m relationship with the Ingredients model
  it { should have_many(:ingredients).dependent(:destroy) }
  # ensure Meal model has a m:m relationship with the Food model
  it { should have_many(:foods) }
  # Validation test
  # ensure column name is present before saving
  it { should validate_presence_of(:name) }
end
