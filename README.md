# React + Redux + Rails (R3) Headless CMS

This project uses React Redux for the frontend client, and a Rails API as the backend headless CMS.  It includes Active
 Admin for administering the site. 
 
The goal of the project is to provide an excellent starting point for any project that would like to use React on the 
froentend with a Rails API as the backend. It uses JWT tokens to secure communication between the client and server.


## Getting Started
You will need to have a few things setup in your dev env in order to get started.

### Development Environment
It is recommended you have RVM or another ruby version manager setup on your system, but this is not a requirement. 

#### Prerequisites
- git
- ruby 2.5.1
- rails 5.2
- yarn
- heroku cli

#### Version Control

This project follows the gitflow workflow. We have 2 release branches, `master` and `develop`. Please branch off the `develop` branch when creating new feature branches.

```
master 
  +-- develop
    +-- feature_branch
```

`master` is always tagged with a semantic version number when deployed.


###### this document is a work in progress - please check back regularly

