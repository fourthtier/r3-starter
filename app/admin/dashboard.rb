ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do
    columns do
      column do
        panel "Recent Articles" do
          ul do
            Article.all.map do |a|
              li link_to(a.title, admin_article_path(a))
            end
          end
        end
      end

      column do
        panel "Info" do
          para "Welcome to ActionTrack Admin area."
        end
      end
    end
  end # content
end
