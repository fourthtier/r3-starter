ActiveAdmin.register Ingredient do
  permit_params :amount, :description, :meal_id, :food_id

end