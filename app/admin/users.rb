ActiveAdmin.register User do
  permit_params :name, :email, :nickname, :image, :sign_in_count, :allow_password_change

  index do
    selectable_column
    column :email
    column :allow_password_change
    actions
  end

  show do
    panel "#{resource.name}" do
      attributes_table_for resource do
        row :provider
        row :uid
        row :reset_password_sent_at
        row :allow_password_change
        row :remember_created_at
        row :current_sign_in_at
        row :current_sign_in_ip
        row :last_sign_in_at
        row :last_sign_in_ip
        row :confirmation_token
        row :unconfirmed_email
        row :nickname
        row :image
        row :email
        row :created_at
        row :updated_at
      end
    end
  end

  form do |f|
    f.inputs "Person Details" do
      f.input :name
      f.input :email
      f.input :nickname
      f.input :image
    end
    f.input :sign_in_count
    f.input :allow_password_change
    f.actions
  end
end
