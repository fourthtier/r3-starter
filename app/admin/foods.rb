ActiveAdmin.register Food do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  permit_params :name,
                :description,
                :serving_size,
                :calories,
                :total_fat,
                :saturated_fat,
                :polyunsaturated_fat,
                :monounsaturated_fat,
                :trans_fat,
                :cholesterol,
                :sodium,
                :potassium,
                :total_carbohydrates,
                :fibre,
                :sugar,
                :protein,
                :tag_list,
                tag_list: []

  index do
    selectable_column
    column :name
    column :total_fat
    column :total_carbohydrates
    column :protein
    column :tag_list
    actions
  end

  show do
    panel "#{resource.name}" do
      attributes_table_for resource do
        row :name
        row :description
        row :tag_list do |t|
          resource.tag_list.split.join(", ")
        end
        row :serving_size
        row :calories
        row :total_fat
        row :saturated_fat
        row :polyunsaturated_fat
        row :monounsaturated_fat
        row :trans_fat
        row :cholesterol
        row :sodium
        row :potassium
        row :total_carbohydrates
        row :fibre
        row :sugar
        row :protein
        row :tag_list
      end
    end
  end

  form do |f|
    f.semantic_errors
    f.inputs do
      f.input :name
      f.input :description
      f.input :tag_list,
              label: 'Tags',
              as: :select,
              value: f.object.tags.pluck(:name),
              collection: Tag.all.pluck(:name),
              input_html: { class: 'select2-food-tags', multiple: 'multiple' }
      f.input :serving_size
      f.input :calories
      f.input :total_fat
      f.input :saturated_fat
      f.input :polyunsaturated_fat
      f.input :monounsaturated_fat
      f.input :trans_fat
      f.input :cholesterol
      f.input :sodium
      f.input :potassium
      f.input :total_carbohydrates
      f.input :fibre
      f.input :sugar
      f.input :protein
    end
    f.actions
  end

end
