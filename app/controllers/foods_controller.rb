class FoodsController < ApiController
  before_action :set_food, only: [:show, :update, :destroy]

  # GET /meals
  def index
    @foods = Food.all
    json_response(@foods)
  end

  # POST /todos
  def create
    @food = Food.create!(food_params)
    json_response(@food, :created)
  end

  # GET /todos/:id
  def show
    json_response(@food)
  end

  # PUT /todos/:id
  def update
    @food.update(food_params)
    head :no_content
  end

  # DELETE /todos/:id
  def destroy
    @food.destroy
    head :no_content
  end

  private

  def food_params
    # whitelist params
    params.permit(:name, :total_fat, :total_carbohydrates, :protein)
  end

  def set_food
    @food = Food.find(params[:id])
  end
end
