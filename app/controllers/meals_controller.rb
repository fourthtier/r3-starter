class MealsController < ApiController
  before_action :set_meal, only: [:show, :update, :destroy]

  # GET /meals
  def index
    @meals = Meal.all
    json_response(@meals)
  end

  # POST /todos
  def create
    @meal = Meal.create!(meal_params)
    json_response(@meal, :created)
  end

  # GET /todos/:id
  def show
    json_response(@meal)
  end

  # PUT /todos/:id
  def update
    @meal.update(meal_params)
    head :no_content
  end

  # DELETE /todos/:id
  def destroy
    @meal.destroy
    head :no_content
  end

  private

  def meal_params
    # whitelist params
    params.permit(:name, :food_id, :meal_id)
  end

  def set_meal
    @meal = Meal.find(params[:id])
  end
end
