class ArticlesController < ApiController
  before_action :authenticate_user!

  def index
    @articles = Article.all
    json_response(@articles)
  end

  # POST /articles
  def create
    @article = Article.create!(safe_params)
    json_response(@article, :created)
  end

  private

  def safe_params
    # whitelist params
    params.permit(:title, :body)
  end

end
