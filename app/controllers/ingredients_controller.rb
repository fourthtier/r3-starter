class IngredientsController < ApiController
  before_action :set_meal
  before_action :set_ingredient, only: [:show, :update, :destroy]

  # GET /meals/:meal_id/ingredients
  def index
    json_response(@meal.ingredients)
  end

  # GET /meals/:meal_id/ingredients/:id
  def show
    json_response(@ingredient)
  end

  # POST /todos/:todo_id/items
  def create
    @meal.ingredients.create!(ingredient_params)
    json_response(@meal, :created)
  end

  # PUT /todos/:todo_id/items/:id
  def update
    @ingredient.update(ingredient_params)
    head :no_content
  end

  # DELETE /todos/:todo_id/items/:id
  def destroy
    @ingredient.destroy
    head :no_content
  end

  private

  def ingredient_params
    params.permit(:amount, :description, :food_id, :meal_id)
  end

  def set_meal
    @meal = Meal.find(params[:meal_id])
  end

  def set_ingredient
    @ingredient = @meal.ingredients.find_by!(id: params[:id]) if @meal
  end
end