class Ingredient < ApplicationRecord
  belongs_to :meal
  belongs_to :food

  validates_presence_of :amount
end
