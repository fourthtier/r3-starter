class Food < ApplicationRecord
  has_many :ingredients
  has_many :meals, through: 'ingredients'
  acts_as_taggable_on :tags

  validates_presence_of :name
  validates_presence_of :total_fat
  validates_presence_of :total_carbohydrates
  validates_presence_of :protein
end
