import axios from 'axios';
import { AUTH_USER, AUTH_ERROR } from "./types";

export const signup = (props, callback) => async dispatch => {
  try {
    const response = await axios.post(
      `${process.env.REACT_APP_API}/auth`,
      props
    );

    const token = {
      token: response.headers['access-token'],
      client: response.headers['client'],
      expiry: response.headers['expiry'],
      uid: response.headers['uid']
    };

    console.log('AUTH: ', token);

    dispatch({ type: AUTH_USER, payload: token });
    // Persist login state
    localStorage.setItem('token', token.token);
    localStorage.setItem('client', token.client);
    localStorage.setItem('expiry', token.expiry);
    localStorage.setItem('uid', token.uid);
    callback();
  } catch (e) {
    console.log(e);
    dispatch({ type: AUTH_ERROR, payload: e.message })
  }
};

export const signin = (props, callback) => async dispatch => {
  try {
    const response = await axios.post(
      `${process.env.REACT_APP_API}/auth/sign_in`,
      props
    );

    const token = {
      token: response.headers['access-token'],
      client: response.headers['client'],
      expiry: response.headers['expiry'],
      uid: response.headers['uid']
    };

    console.log('AUTH: ', token);

    dispatch({ type: AUTH_USER, payload: token });
    // Persist login state
    localStorage.setItem('token', token.token);
    localStorage.setItem('client', token.client);
    localStorage.setItem('expiry', token.expiry);
    localStorage.setItem('uid', token.uid);
    callback();
  } catch (e) {
    console.log(e);
    dispatch({ type: AUTH_ERROR, payload: e.message })
  }
};

export const signout = () => {
  localStorage.removeItem('token');

  return {
    type: AUTH_USER,
    payload: ''
  }
};

export const getMeals = (props) => dispatch => {
  axios.get(`${process.env.REACT_APP_API}/meals`);
};

export const getArticles = (callback) => dispatch => {
  let token = localStorage.getItem('token');
  let client = localStorage.getItem('client');
  let expiry = localStorage.getItem('expiry');
  let uid = localStorage.getItem('uid');

  if (token) {
   try {
     let axiosConfig = {
       headers: {
         'Content-Type': 'application/json;charset=UTF-8',
         'Access-Control-Allow-Headers': '*',
         'access-token': token,
         'client': client,
         'expiry': expiry,
         'uid': uid
       }
     };

     axios.get(
        `${process.env.REACT_APP_API}/articles`,
        axiosConfig)

    } catch (e) {
      console.log("ERROR: ", e);
    }

  } else {
    console.log("NO TOKEN SET");
  }

};