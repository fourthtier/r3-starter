import React from 'react'

import Header from './Header'

export default ({children}) => {
  return (
    <main>
      <Header />
      {children}
    </main>
  )
}