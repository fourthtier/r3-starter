import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from "../actions";

import './Header.css';

class Header extends Component {

  renderLinks() {
    if (this.props.authenticated) {
      return (
        <div className="app-header__menu">
          <Link to="/dashboard">Dashboard</Link>
          <Link to="/sign-out">Sign Out</Link>
        </div>
      )
    } else {
      return (
        <div className="app-header__menu">

          <Link to="/sign-up">Sign Up</Link>
          <Link to="/sign-in">Sign In</Link>
        </div>
      )
    }
  }

  render() {
    return (
      <header className="app-header">
        <div className="app-header__brand">
          <span className="app-header__logo">R3::CMS</span>
          <Link to="/">Home</Link>
        </div>
        {this.renderLinks()}
      </header>
    )
  }
}

const mapStateToProps = (state) => {
  return { authenticated: state.auth.authenticated };
};

export default connect(mapStateToProps, actions)(Header);
