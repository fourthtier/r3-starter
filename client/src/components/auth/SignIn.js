import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';
import { compose } from 'redux';
import { connect } from 'react-redux';
import * as actions from '../../actions';

class SignIn extends Component {
  onSubmit = (formProps) => {

    this.props.signin(formProps, () => {
      this.props.history.push('/dashboard')
    });
  };

  onGetArticles = (e) => {
    e.preventDefault();
    this.props.getArticles(null, () => {
      console.log("BACK")
    });
  };

  render() {
    const { handleSubmit } = this.props;

    return (
      <div className="container">
        <div>
          {this.props.errorMessage}
        </div>
        <form onSubmit={handleSubmit(this.onSubmit)}>
          <fieldset>
            <label>Email</label>
            <Field
              name="email"
              type="text"
              component="input"
              autoComplete="off"
            />
          </fieldset>
          <fieldset>
            <label>Password</label>
            <Field
              name="password"
              type="password"
              component="input"
              autoComplete="off"
            />
          </fieldset>
          <button type="submit">Sign In!</button>
          <button onClick={this.onGetArticles}>Get Articles</button>
        </form>

      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return { errorMessage: state.auth.errorMessage };
}


export default compose(
  connect(mapStateToProps, actions),
  reduxForm({ form: 'signin' }),
)(SignIn)
