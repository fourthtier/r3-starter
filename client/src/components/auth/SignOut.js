import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions'

class SignOut extends Component {
  componentDidMount() {
    this.props.signout();
  }

  render() {
    return (
      <div className="container">
        <h2>You have successfully signed out! Hope to see you soon.</h2>
      </div>
    )
  }
}

export default connect(null, actions)(SignOut);