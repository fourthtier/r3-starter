import React from 'react';

export default() => {
  return (
    <div className="container">
      <h1>React + Redux + Rails Starter</h1>
      <h3>Headless CMS:</h3>
      <ul>
        <li>Rails RestfulAPI as Service Layer</li>
        <li>SEO Friendly / Optimized</li>
        <li>Active Admin for CMS backend</li>
        <li>Authentication:
          <ul>
            <li>Devise to authenticate Active Admin users with sessions</li>
            <li>Devise Token Auth to authenticate client requests with JWT tokens</li>
          </ul>
        </li>
        <li>React + Redux Frontend</li>
        <li>Single git repository</li>
      </ul>
    </div>
  )
}