import React, { Component } from 'react';
import requireAuth from './auth/requireAuth';
import * as actions from "../actions";
import {connect} from "react-redux";

class Dashboard extends Component {

  onGetArticles = () => {
    this.props.getArticles(null, () => {
      console.log("CHA CHA CHA!")
    });
  };

  render() {
    return (
      <div className="container">
        THIS IS THE PRIVATE DASHBOARD AND SHOULD REQUIRE SIGNIN TO VIEW!

        <button onClick={this.onGetArticles}>Get Articles!</button>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    authenticated: state.auth.authenticated,
    errorMessage: state.auth.errorMessage
  };
}

export default connect(mapStateToProps, actions)( requireAuth(Dashboard) );
