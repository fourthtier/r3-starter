import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reduxThunk from 'redux-thunk';

import reducers from './reducers';
import App from './components/App';
import Welcome from './components/Welcome';
import SignUp from './components/auth/SignUp';
import SignOut from './components/auth/SignOut';
import SignIn from './components/auth/SignIn';
import Dashboard from './components/Dashboard';

import './index.css';

const store = createStore(
  reducers,
  {
    auth: { authenticated: localStorage.getItem('token') }
  },
  applyMiddleware(reduxThunk)
)

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App>
        <Route path="/" exact component={Welcome} />
        <Route path="/sign-up"  component={SignUp} />
        <Route path="/dashboard" component={Dashboard} />
        <Route path="/sign-out" component={SignOut} />
        <Route path="/sign-in" component={SignIn} />
      </App>
    </BrowserRouter>
  </Provider>
  , document.querySelector('#root')
);