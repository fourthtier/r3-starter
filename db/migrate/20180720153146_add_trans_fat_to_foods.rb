class AddTransFatToFoods < ActiveRecord::Migration[5.2]
  def change
    add_column :foods, :trans_fat, :float
  end
end
