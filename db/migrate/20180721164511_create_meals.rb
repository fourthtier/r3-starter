class CreateMeals < ActiveRecord::Migration[5.2]
  def change
    create_table :meals do |t|
      t.string :name
      t.string :description
      t.string :steps
      t.string :source
      t.timestamps
    end
  end
end
