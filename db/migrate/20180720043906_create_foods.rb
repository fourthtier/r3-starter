class CreateFoods < ActiveRecord::Migration[5.2]
  def change
    create_table :foods do |t|
      t.string :name
      t.string :description
      t.float :serving_size
      t.float :calories
      t.float :total_fat
      t.float :saturated_fat
      t.float :polyunsaturated_fat
      t.float :monounsaturated_fat
      t.float :cholesterol
      t.float :sodium
      t.float :potassium
      t.float :total_carbohydrates
      t.float :fibre
      t.float :sugar
      t.float :protein

      t.timestamps
    end
  end
end
