# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password') if Rails.env.development?

Food.create!(name: 'Avocado',
             description: 'The avocado is a tree, long thought to have originated in South Central Mexico, classified as a member of the flowering plant family Lauraceae. Wikipedia',
             serving_size: 100,
             total_fat: 15,
             saturated_fat: 2.1,
             polyunsaturated_fat: 1.8,
             monounsaturated_fat: 10,
             trans_fat: 0,
             cholesterol: 0,
             sodium: 7,
             potassium: 485,
             total_carbohydrates: 9,
             protein: 2)

Food.create!(name: 'Sunbrown Calrose Brown Rice',
             description: 'The best under the Australian Sun',
             serving_size: 45,
             calories: 160,
             total_fat: 1,
             total_carbohydrates: 32,
             fibre: 1,
             protein: 3)